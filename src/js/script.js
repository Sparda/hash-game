let content = document.querySelector("#content");
let div_table = document.querySelector("#table");
let start_bt = document.querySelector("#start");
let restart_bt = document.querySelector("#restart");
let message = document.querySelector("#message");
let moves = 0;

start_bt.addEventListener('click', () => {
    restart_bt.classList.remove("hide")
    start_bt.classList.add("hide")
    printGame();
})

restart_bt.addEventListener('click', () => {
    message.innerText = "";
    restartGame();
});

function printGame() {
    moves % 2 == 0 || moves == 0 ? message.innerText = "Jogador: o" : message.innerText = "Jogador: x";
    div_table.append(createTable(3, 3));
}

function restartGame() {
    moves = 0;
    div_table.innerHTML = ""
    moves % 2 == 0 || moves == 0 ? message.innerText = "Jogador: o" : message.innerText = "Jogador: x";
    div_table.append(createTable(3, 3));
}

function createTable(cols, rows) {
    let table = document.createElement('table');
    table.id = "squareGrid";
    for (var i = 0; i < rows; i++) {
        let row = document.createElement('tr');
        table.append(row);
        for (var j = 0; j < cols; j++) {
            let col = document.createElement('td');
            col.id = i * cols + j;
            col.addEventListener('click', () => {
                if (!hasWinner() && col.innerText == "") {
                    moves % 2 == 0 || moves == 0 ? col.innerText = 'o' : col.innerText = 'x';
                    moves++;
                    gameStatus();
                }
            });
            row.append(col)
        }
    }
    return table;
}

function gameStatus() {
    console.log([hasWinner, moves])
    if (moves == 9 && !hasWinner()) {
        message.innerText = "Empate!"
    } else if (hasWinner()) {
        moves % 2 == 0 || moves == 0 ? message.innerText = "Vencedor: o" : message.innerText = "Vencedor: x"
    } else {
        moves % 2 == 0 || moves == 0 ? message.innerText = "Jogador: o" : message.innerText = "Jogador: x";
    }
}

function hasWinner() {
    if (verifyDiagonals() || verifyLines()) {
        return true;
    } else {
        return false
    }
}

function verifyDiagonals() {
    let table = document.querySelector("#squareGrid");
    let cels = table.getElementsByTagName("td");
    let response = false;
    let possibleWins = ["048", "246"];
    for (var i = 0; i < possibleWins.length; i++) {
        let actualCels = new Array(3);
        for (var j = 0; j < possibleWins[i].length; j++) {
            actualCels[j] = possibleWins[i].charAt(j);
        }
        if (cels[actualCels[0]].innerText == cels[actualCels[1]].innerText && cels[actualCels[2]].innerText == cels[actualCels[1]].innerText && cels[actualCels[0]].innerText != "") {
            response = true;
            break;
        }
    }
    return response;
}

function verifyLines() {
    let table = document.querySelector("#squareGrid");
    let cels = table.getElementsByTagName("td");
    let response = false;
    let possibleWins = ["012", "345", "678", "036", "147", "258"];
    for (var i = 0; i < possibleWins.length; i++) {
        let actualCels = new Array(3);
        for (var j = 0; j < possibleWins[i].length; j++) {
            actualCels[j] = possibleWins[i].charAt(j);
        }
        if (cels[actualCels[0]].innerText == cels[actualCels[1]].innerText && cels[actualCels[2]].innerText == cels[actualCels[1]].innerText && cels[actualCels[0]].innerText != "") {
            response = true;
            break;
        }
    }
    return response;
}